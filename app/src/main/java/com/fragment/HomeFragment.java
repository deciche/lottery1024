package com.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.MyApp;
import com.activity.LotteryRoomActivity;
import com.bean.AllListBean;
import com.constant.Constant;
import com.lottery.R;
import com.util.BitmapUtils;
import com.util.ToastUtils;
import com.youth.banner.Banner;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class HomeFragment extends BaseFragment {

    private View inflate;
    private List<AllListBean.DataBean> mListLottery;
    private MyAdapter myAdapter;

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        inflate = inflater.inflate(R.layout.fragment_home, container, false);
        initData();
        initUI(inflate);
        return inflate;
    }

    private void initData() {
        mListLottery = new ArrayList<>();
    }


    private void initUI(View inflate) {
        Banner mBanner = inflate.findViewById(R.id.banner);
        GridView mGrid = inflate.findViewById(R.id.gridView);
        myAdapter = new MyAdapter(mListLottery);
        mGrid.setAdapter(myAdapter);
        mGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                AllListBean.DataBean dataBean = mListLottery.get(i);
                Intent intent = new Intent(getActivity(), LotteryRoomActivity.class);
                intent.putExtra(Constant.LEVEL_LIST, dataBean);
                startActivity(intent);
            }
        });
    }

    public void notifyDataChange(List<AllListBean.DataBean> listLottery) {
        this.mListLottery = listLottery;
        myAdapter.notifyDataSetChanged();
    }

    class MyAdapter extends BaseAdapter {

        List<AllListBean.DataBean> list;

        public MyAdapter(List<AllListBean.DataBean> listLottery) {
            list = listLottery;
        }

        @Override
        public int getCount() {
            return mListLottery.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {
            View inflate = getActivity().getLayoutInflater().inflate(R.layout.item_grid_home, null);
            ImageView imgRoom = inflate.findViewById(R.id.imgRoom);
            BitmapUtils.getInstance().display(MyApp.getInstance(), imgRoom, mListLottery.get(i).getPicurl());
            inflate.findViewById(R.id.imgRoomShows).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ToastUtils.showToast("Position = " + i);
                }
            });

            return inflate;
        }
    }
}
