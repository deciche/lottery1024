package com.util;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.Toast;

import com.MyApp;
import com.custom.AlertDialog;
import com.lottery.R;

/**
 * 同统一提示窗口AlertDialog
 *
 * @author Administrator
 */
public class DialogIos {

    private static EditText control;
    private static boolean msgzhi = false;

    public static void show(FragmentActivity fragmentActivity, String msg) { // 得到Builder对象
        Toast.makeText(fragmentActivity, "" + msg, Toast.LENGTH_LONG).show();
    }

    public static void show3(Activity activity, String msg) { // 得到Builder对象
        Toast.makeText(activity, "" + msg, Toast.LENGTH_LONG).show();
    }

    public static void show_edit(final Context context,
                                 FragmentActivity fragmentActivity, String msg, final EditText et) { // 得到Builder对象
    }

    /**
     * 设置焦点并弹窗
     *
     * @param msg
     * @param mm  EditText获取焦点用
     */
    public static void tan(Context context, String msg, final EditText mm) {
        AlertDialog dialog = new AlertDialog(context).builder()
                .setTitle(context.getResources().getString(R.string.reminder))
                .setMsg(msg)
                .setPositiveButton("确定", new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mm != null) {
                            mm.setEnabled(true);
                            mm.setFocusable(true);
                            mm.requestFocus();
                            mm.setSelection(mm.length());
                            Utils.showSoftKeyBoard(MyApp.getInstance(), mm);
                        }
                    }
                });
        dialog.show();
    }

    /**
     * 设置焦点并弹窗
     *
     * @param msg
     */
    public static void tan(Context context, String msg) {
        AlertDialog dialog = new AlertDialog(context).builder()
                .setTitle(context.getResources().getString(R.string.reminder))
                .setMsg(msg)
                .setPositiveButton("确定", new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    }
                });
        dialog.show();
    }

}
