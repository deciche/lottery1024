package com.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.MyApp;


public class NetworkUtil {
    public static boolean isNetworkAvailable() {
        Context ctx = MyApp.getInstance();
        ConnectivityManager connectivity = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivity == null) {
            return false;
        } else {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();

            if (info != null) {
                //NetworkInfo: type(0--mobile[UMTS], 1--WIFI[],
                //2--mobile_mms[UNKNOWN]), 3--mobile_supl[UNKNOWN]
                //4--mobile_hipri[UNKNOWN]
                for (int i = 0; i < info.length; i++) {
                    if ((info[i].getType() == 0 || info[i].getType() == 1)
                            && info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

}
