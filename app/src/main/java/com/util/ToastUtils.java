package com.util;

import android.widget.Toast;

import com.MyApp;

/**
 * Created by Mars on 2017/9/20.
 */

public class ToastUtils {

    private static Toast mToast = null;
    private static long firstTime = 0;
    private static long secondTime;
    private static String oldMsg;
    private static int oldMsg1;

    public static void showToast(String message) {

        if (mToast == null) {
            mToast = Toast.makeText(MyApp.getInstance(), message, Toast.LENGTH_SHORT);
            mToast.show();
            firstTime = System.currentTimeMillis();
        } else {
            secondTime = System.currentTimeMillis();
            if (message.equals(oldMsg) && (secondTime - firstTime) > Toast.LENGTH_SHORT) {
                mToast.show();
            } else {
                oldMsg = message;
                mToast.setText(message);
                mToast.show();
            }
        }
        firstTime = secondTime;
    }

    public static void showToast(int message) {

        if (mToast == null) {
            mToast = Toast.makeText(MyApp.getInstance(), message, Toast.LENGTH_SHORT);
            mToast.show();
            firstTime = System.currentTimeMillis();
        } else {
            secondTime = System.currentTimeMillis();
            if ((message == oldMsg1) && (secondTime - firstTime) > Toast.LENGTH_SHORT) {
                mToast.show();
            } else {
                oldMsg1 = message;
                mToast.setText(message);
                mToast.show();
            }
        }
        firstTime = secondTime;
    }

}
