package com.util;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.MyApp;

/**
 * Created by Mars on 2017/9/22.
 */

public class Utils {

    private static MyApp instance;

    /**
     * 设置没有状态栏、全屏
     *
     * @param activity
     */
    public static void setNoTitleAndFullScreen(Activity activity) {
        activity.requestWindowFeature(Window.FEATURE_NO_TITLE);
        activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    /**
     * 收起键盘
     *
     * @param act
     */
    public static void KeyBoardCancel(Activity act) {
        View view = act.getWindow().peekDecorView();
        if (view != null) {
            InputMethodManager inputManger = (InputMethodManager) act
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManger.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void showSoftKeyBoard(Context context, View v) {
        try {
            final InputMethodManager imm = (InputMethodManager) context
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            // 因为AlertDialog的点击事件和dismiss问题，所以要加延迟；
//            new Handler().postDelayed(new Runnable() {
//
//                @Override
//                public void run() {

            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, InputMethodManager.HIDE_NOT_ALWAYS);

//                }
//            }, 50);
        } catch (Exception e) {
        }
    }


    public static String getVersionName() {
        instance = MyApp.getInstance();
        PackageManager manager = instance.getPackageManager();
        PackageInfo packageInfo = null;
        try {
            packageInfo = manager.getPackageInfo(instance.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return packageInfo.versionName;
    }
}
