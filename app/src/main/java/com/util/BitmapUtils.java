package com.util;

import android.content.Context;
import android.widget.ImageView;

import com.MyApp;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.Target;

import java.io.File;

public class BitmapUtils {

	private BitmapUtils() {
	}

	private static BitmapUtils mBitmapUtils;

	public static BitmapUtils getInstance() {
		if (mBitmapUtils == null) {
			mBitmapUtils = new BitmapUtils();
		}
		return mBitmapUtils;
	}

	// 普通显示图片
	public void display(Context ctx, ImageView iv, String url) {
		Glide.with(ctx).load(url).into(iv);
	}

	// 显示高质量图片
	public void displayHigh(ImageView iv, String url) {
		Glide.with(MyApp.getInstance()).load(url).into(iv);
	}
	
	// 显示圆角图片
//	public void displayWithRound(ImageView iv, String url, int round) {
//		Glide.with(MyApp.getInstance()).load(url).transform(new GlideRoundTransform(MyApp.getInstance(), round)).into(iv);
//	}
	
	// 显示高质量图片,fitxy
	public void displayHighFitXY(ImageView iv, String url) {
		Glide.with(MyApp.getInstance()).load(url).into(iv);
	}

	// 清除缓存
	public void clearCache(final Context ctx) {
		new Thread(){
			@Override
			public void run() {
				Glide.get(ctx).clearDiskCache();
			}
		}.start();
		Glide.get(ctx).clearMemory();
	}

	//下载图片
	public void loadPic(final Context ctx, final String url, final ImageDownLoadCallBack callBack){
		new Thread(){
			public void run() {
				File file = null;
		        try {
		            file = Glide.with(ctx)
		                    .load(url)
		                    .downloadOnly(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
		                    .get();
		        } catch (Exception e) {
		            e.printStackTrace();
		        } finally {
		            if (file != null) {
		                callBack.onDownLoadSuccess(file);
		            } else {
		                callBack.onDownLoadFailed();
		            }
		        }
			}
		}.start();
	}
	
	//图片下载回调接口
	public static interface ImageDownLoadCallBack {
	    void onDownLoadSuccess(File file);
	    void onDownLoadFailed();
	}
}
