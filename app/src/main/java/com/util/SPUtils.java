package com.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.MyApp;

/**
 * Created by Mars on 2017/9/22.
 */

public class SPUtils {
    private static String FILE_NAME = "sharePreferences";

    public SPUtils() {
    }

    private static SharedPreferences getSp() {
        return MyApp.getInstance().getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
    }

    public static String getString(String key, String def) {
        return getSp().getString(key, def);
    }

    public static boolean getBoolean(String key, boolean def) {
        return getSp().getBoolean(key, def);
    }

    public static void put(String key, Object b) {
        SharedPreferences.Editor edit = getSp().edit();
        if (b instanceof String)
            edit.putString(key, (String) b).commit();
        if (b instanceof Integer)
            edit.putInt(key, (Integer) b).commit();
        if (b instanceof Float)
            edit.putFloat(key, (Float) b).commit();
        if (b instanceof Boolean)
            edit.putBoolean(key, (Boolean) b).commit();
        if (b instanceof Long)
            edit.putLong(key, (Long) b).commit();
    }
}
