package com.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 正则表达式检验
 * 
 * @author lulu
 * 
 */
public class CheckAvailUtil {
	/**
	 * 验证手机号码是否有效
	 */
	public static boolean mobileNum(String mobiles) {
		Pattern p = Pattern.compile("^((13[0-9])|(15[^4,\\D])|(17[0-9])|(18[0,0-9]))\\d{8}$");
		Matcher m = p.matcher(mobiles);
		return m.matches();
	}

	/**
	 * 验证是否时间格式
	 */
	public static boolean checkDate(String date) {
		Pattern p = Pattern.compile("^\\s?\\d{4}.?\\d{1,2}.?\\d{1,2}(.?\\d{1,2}.?\\d{1,2}.?\\d{1,2})?\\s?$");
		Matcher m = p.matcher(date);
		return m.matches();
	}

	/**
	 * 身份证号码：18纯数字或17+x
	 */
	public static boolean checkId(String id) {
		Pattern p = Pattern.compile("^[0-9]{18}|[0-9]{17}[x]{1}$");
		Matcher m = p.matcher(id);
		return m.matches();
	}

	/**
	 * 验证银行卡号
	 * 
	 * @param bankId
	 * @return
	 */
	public static boolean isBankId(String bankId) {
		Pattern p = Pattern.compile("^(\\d{16}|\\d{19})$");
		Matcher m = p.matcher(bankId);
		return m.matches();
	}
}
