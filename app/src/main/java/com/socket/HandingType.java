package com.socket;

/**
 * Created by Mars on 2017/10/17.
 */

public class HandingType {
    private byte[] head;
    private byte[] body;

    public byte[] getHead() {
        return head;
    }

    public void setHead(byte[] head) {
        this.head = head;
    }

    public byte[] getBody() {
        return body;
    }

    public void setBody(byte[] body) {
        this.body = body;
    }
}
