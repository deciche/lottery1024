package com.socket;

import android.util.Base64;

import com.bean.DataEvent;
import com.constant.PackageType;
import com.event.MapInstance;
import com.util.DataTypeUtil;
import com.util.LogUtil;

import java.util.HashMap;

import de.greenrobot.event.EventBus;

/**
 * Created by Mars on 2017/10/17.
 */

public class FactoryType {

    private FactoryType factoryType;

    public FactoryType() {

    }


    public HashMap<Integer, byte[]> map = new HashMap<>();
    public String str = "";
    public StringBuffer stringBuffer = new StringBuffer();

    public BaseType createTypeSingle(int cmd, byte[] unPacket) {
        switch (cmd) {
            case PackageType.USER_LOGIN:
                return new Login(unPacket);
            case PackageType.GET_BANNER_LIST:
                String string = DataTypeUtil.getString(unPacket);
                LogUtil.e(string);
                return null;
        }
        return null;
    }

    /**
     * 生成不同类型的实体
     *
     * @param cmd  功能码
     * @param tag  包序号
     * @param body 包体
     * @return
     */
    public BaseType createType(int cmd, int tag, byte[] body) {
        switch (cmd) {
            case PackageType.GET_ALL_LIST:
                int numCurrent = tag & 0x00ff;
                int numAll = tag >> 8;
                if (numCurrent != numAll) {

                }
        }
        return null;
    }

    /**
     * 多包
     *
     * @param cmd  功能码
     * @param tag
     * @param body
     * @return
     */
    public BaseType createTypeMultiple(int cmd, int tag, byte[] body) {

        MapInstance mapInstance = MapInstance.getMapInstance();
        StringBuffer stringBuffer = mapInstance.getStringBuffer(cmd);

        switch (cmd) {
            case PackageType.GET_ALL_LIST:
                int numCurrent = tag & 0x00ff;
                int numAll = tag >> 8;
                if (body != null && numCurrent != 0)
                    map.put(numCurrent, body);
                stringBuffer.append(new String(body));
                String message = stringBuffer.toString();
                if (numCurrent == numAll) {
                    byte[] decode = Base64.decode(stringBuffer.toString().getBytes(), Base64.DEFAULT);
                    String s = new String(decode);
//                    LogUtil.e(s);
                    DataEvent dataEvent = new DataEvent();
                    dataEvent.setStr(s);
                    EventBus.getDefault().post(dataEvent);
                }
                break;
        }
        return null;
    }

    /**
     * @param cmd
     * @param unPacket
     */
    public void createCmd(int cmd, UnPacket unPacket) {
        switch (cmd) {
            case PackageType.USER_LOGIN:

                break;
        }
    }
}
