package com.socket;

import android.util.Base64;

import com.bean.LoginSuccessBean;
import com.util.JsonUtils;
import com.util.LogUtil;

import java.util.ArrayList;

/**
 * Created by Mars on 2017/10/17.
 */
public abstract class BaseType {
    public byte[] body;

    abstract void run();

}

class GetAllList extends BaseType {
    GetAllList(ArrayList unPacket) {

    }

    @Override
    void run() {

    }
}

class Login extends BaseType {

    Login(byte[] body) {
        this.body = body;
    }

    @Override
    void run() {
//        if (tag == 0) {
//            //单包
//            BaseType type = factory.createTypeSingle(cmd, body);
//        } else {
//            //多包
//            factory.createTypeMultiple(cmd, tag, body);
//        }
        LoginSuccessBean result = JsonUtils.getByteJsonResult(body, LoginSuccessBean.class);
        String code = result.getCode();
        LogUtil.e("code = " + code);
    }
}