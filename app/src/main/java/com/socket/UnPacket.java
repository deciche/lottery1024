package com.socket;

public class UnPacket {
    public byte[] head;
    public byte[] body;

    public UnPacket(byte[] head, byte[] body) {
        this.head = head;
        this.body = body;
    }

    public void Parser() {
        //功能码
        int cmd = head[5] << 8 | head[4];
        //包序号
        int tag = head[7] << 8 | head[6];
        // TODO: 2017/10/18 事件分发 根据“功能码”分发事件

        FactoryType factory = new FactoryType();
        if (tag == 0) {
            //单包
                    factory.createTypeSingle(cmd, body);
        } else {
            //多包
            factory.createTypeMultiple(cmd, tag, body);
        }
//        factory.createCmd(cmd, this);  //
    }
}
