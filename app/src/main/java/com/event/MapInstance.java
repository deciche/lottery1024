package com.event;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by lulu on 2017/10/18.
 */

public class MapInstance {
    private Map<Short, StringBuffer> map;
    private static MapInstance mapInstance;

    public static MapInstance getMapInstance() {
        if (mapInstance == null) {
            mapInstance = new MapInstance();
        }
        return mapInstance;
    }

    public Map getInstance() {
        if (map == null) {
            map = new HashMap<>();
        }
        return map;
    }

    public StringBuffer getStringBuffer(int msgType) {
        Map map = getInstance();
        StringBuffer stringBuffer = (StringBuffer) map.get(msgType);
        if (stringBuffer == null) {
            stringBuffer = new StringBuffer();
            map.put(msgType, stringBuffer);
        }
        return stringBuffer;
    }
}
