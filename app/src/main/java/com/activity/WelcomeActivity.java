package com.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.MainPresenter;
import com.MyApp;
import com.constant.Constant;
import com.interfaces.IEvent;
import com.interfaces.IEventListener;
import com.lottery.R;
import com.socket.SendThread;
import com.util.LogUtil;
import com.util.SPUtils;
import com.util.Utils;

import java.io.IOException;
import java.net.Socket;

public class WelcomeActivity extends Activity implements IEventListener {

    private MainPresenter mMP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Utils.setNoTitleAndFullScreen(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

//        mMP = MainPresenter.getInstance();
//        mMP.registerEventListener(this, this);
//        mMP.startService();
        LogUtil.e(MyApp.getInstance().getString(R.string.please_fill_recID));

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                turnActivity();
            }
        }, Constant.ENTER_TIME_DELAY);

    }

    private void turnActivity() {
        String versionName = Utils.getVersionName();
        boolean isGuide = SPUtils.getBoolean(versionName, true);
        if (isGuide) {
            SPUtils.put(versionName, false);
            startActivity(new Intent(this, GuideActivity.class));
            finish();
        } else {
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        return;
    }

    @Override
    public boolean onEvent(IEvent iEvent) {

        return false;
    }

    @Override
    protected void onDestroy() {
//        mMP.unregisterEventListener(this);
        super.onDestroy();
    }
}
