package com.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.MyApp;
import com.bean.AllListBean;
import com.constant.Constant;
import com.interfaces.IEvent;
import com.lottery.R;
import com.util.BitmapUtils;
import com.util.StatusBarUtil;
import com.util.ToastUtils;

import java.io.Serializable;
import java.util.List;

public class LotteryRoomActivity extends BaseActivity implements View.OnClickListener {

    private AllListBean.DataBean mDataBean;
    private int[] stateList;
    private String strCanada28;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lottery_room);
        StatusBarUtil.setColor(this);

        initData();
        initUI();
    }

    private void initUI() {
        findViewById(R.id.btnBack).setOnClickListener(this);
        findViewById(R.id.btnService).setOnClickListener(this);
        TextView mTitle = findViewById(R.id.textTitle);
        mTitle.setText(strCanada28);
        final ListView mListView = findViewById(R.id.listRoom);
        MyAdapter myAdapter = new MyAdapter(mDataBean.getLevels());
        mListView.setAdapter(myAdapter);

        final GridView mGridView = findViewById(R.id.gridView);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                ToastUtils.showToast("ClickHere");
                AllListBean.DataBean.LevelsBean levelsBean = mDataBean.getLevels().get(i);
                Intent intent = new Intent(LotteryRoomActivity.this, InnerRoomListActivity.class);
                String[] strNames = {strCanada28, levelsBean.getLevelname()};
                intent.putExtra(Constant.ROOM_NAME, strNames);
                intent.putExtra(Constant.LEVEL_BEAN, levelsBean);
                startActivity(intent);
            }
        });
    }

    class MyAdapter extends BaseAdapter {

        List<AllListBean.DataBean.LevelsBean> levels;

        public MyAdapter(List<AllListBean.DataBean.LevelsBean> levels) {
            this.levels = levels;
        }

        @Override
        public int getCount() {
            return mDataBean.getLevels().size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View inflate = getLayoutInflater().inflate(R.layout.item_lottery_room, null);
            ImageView imgRoom = inflate.findViewById(R.id.imgRoom);
            ImageView imgState = inflate.findViewById(R.id.imgCompensateState);
            BitmapUtils.getInstance().display(MyApp.getInstance(), imgRoom, levels.get(i).getPicurl());
            imgState.setImageResource(stateList[i]);
            imgState.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // TODO: 2017/10/20 赔率说明界面跳转
                    ToastUtils.showToast("Todo");
                }
            });
            return inflate;
        }
    }

    private void initData() {
        Intent intent = getIntent();
        mDataBean = (AllListBean.DataBean) intent.getSerializableExtra(Constant.LEVEL_LIST);
        strCanada28 = mDataBean.getName();
        stateList = new int[]{R.mipmap.compensate_state1, R.mipmap.compensate_state2,
                R.mipmap.compensate_state3};
    }

    @Override
    public boolean onEvent(IEvent iEvent) {
        return false;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:
                finish();
                break;
            case R.id.btnService:
                ToastUtils.showToast("Todo");
                break;
        }
    }
}
