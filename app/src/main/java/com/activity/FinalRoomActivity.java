package com.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.bean.AllListBean;
import com.constant.Constant;
import com.interfaces.IEvent;
import com.lottery.R;
import com.lqr.emoji.EmotionKeyboard;
import com.lqr.emoji.EmotionLayout;
import com.lqr.emoji.IEmotionExtClickListener;
import com.lqr.emoji.IEmotionSelectedListener;
import com.util.StatusBarUtil;
import com.util.ToastUtils;

public class FinalRoomActivity extends BaseActivity implements View.OnClickListener, IEmotionSelectedListener {

    public static final int NEWS_MESSAGE_TEXT = 100;//通知公告信息
    private String[] roomNames;
    private int index = 0;

    private Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case NEWS_MESSAGE_TEXT:
                    textSwitcher.setText(roomNames[index % roomNames.length]);
                    index++;
                    if (index == 3) {
                        index = 0;
                    }
                    break;
                default:
                    break;
            }
        }
    };
    private TextSwitcher textSwitcher;
    private EmotionLayout mElEmotion;
    private EditText mEtContent;
    private EmotionKeyboard mEmotionKeyboard;
    private View mLlContent;
    private FrameLayout mFlEmotionView;
    private ImageView mIvEmo;
    private ImageView mIvMore;
    private LinearLayout mLlMore;
    private AllListBean.DataBean.LevelsBean.RoomsBean roomsBean;
    private Button mBtnSend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final_room);
        StatusBarUtil.setColor(this);
        initData();
        initUI();
        initListener();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mEtContent.clearFocus();
    }

    private void initUI() {
        textSwitcher = findViewById(R.id.textSwitcher);
        setSwitcher();

        findViewById(R.id.btnBack).setOnClickListener(this);
        findViewById(R.id.btnService).setOnClickListener(this);
        findViewById(R.id.imgMenu).setOnClickListener(this);
        findViewById(R.id.layoutRecentPeriod).setOnClickListener(this);

        // 设置表情
        mEtContent = findViewById(R.id.etContent);
        mElEmotion = findViewById(R.id.elEmotion);
        mElEmotion.setEmotionSelectedListener(this);
        mElEmotion.attachEditText(mEtContent);

        mLlContent = findViewById(R.id.llContent);
        mFlEmotionView = findViewById(R.id.flEmotionView);

        mIvEmo = findViewById(R.id.ivEmo);// 表情
        mIvMore = findViewById(R.id.ivMore);//Add
        mLlMore = findViewById(R.id.llMore);

        mBtnSend = findViewById(R.id.btnSend);

        initEmotionKeyboard();
    }


    /**
     * 下面聊天事件监听
     */
    private void initListener() {
        mElEmotion.setEmotionSelectedListener(this);
        mElEmotion.setEmotionAddVisiable(true);
        mElEmotion.setEmotionSettingVisiable(true);
        mElEmotion.setEmotionExtClickListener(new IEmotionExtClickListener() {
            @Override
            public void onEmotionAddClick(View view) {
                Toast.makeText(getApplicationContext(), "add", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onEmotionSettingClick(View view) {
                Toast.makeText(getApplicationContext(), "setting", Toast.LENGTH_SHORT).show();
            }
        });
        mLlContent.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        closeBottomAndKeyboard();
                        break;
                }
                return false;
            }
        });
        mEtContent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (mEtContent.getText().toString().trim().length() > 0) {
                    mBtnSend.setVisibility(View.VISIBLE);
                    mIvMore.setVisibility(View.GONE);
                } else {
                    mBtnSend.setVisibility(View.GONE);
                    mIvMore.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mBtnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEtContent.setText("");
                Toast.makeText(getApplicationContext(), "发送成功", Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * 设置表情
     */
    private void initEmotionKeyboard() {
        mEmotionKeyboard = EmotionKeyboard.with(this);
        mEmotionKeyboard.bindToEditText(mEtContent);
        mEmotionKeyboard.bindToContent(mLlContent);
        mEmotionKeyboard.setEmotionLayout(mFlEmotionView);
        mEmotionKeyboard.bindToEmotionButton(mIvEmo, mIvMore);
        mEmotionKeyboard.setOnEmotionButtonOnClickListener(new EmotionKeyboard.OnEmotionButtonOnClickListener() {
            @Override
            public boolean onEmotionButtonOnClickListener(View view) {
                switch (view.getId()) {
                    case R.id.ivEmo:
                        if (!mElEmotion.isShown()) {
                            if (mLlMore.isShown()) {
                                showEmotionLayout();
                                hideMoreLayout();
//                                hideAudioButton();
                                return true;
                            }
                        } else if (mElEmotion.isShown() && !mLlMore.isShown()) {
                            mIvEmo.setImageResource(R.mipmap.ic_cheat_emo);
                            return false;
                        }
                        showEmotionLayout();
                        hideMoreLayout();
//                        hideAudioButton();
                        break;
                    case R.id.ivMore:
                        if (!mLlMore.isShown()) {
                            if (mElEmotion.isShown()) {
                                showMoreLayout();
                                hideEmotionLayout();
//                                hideAudioButton();
                                return true;
                            }
                        }
                        showMoreLayout();
                        hideEmotionLayout();
//                        hideAudioButton();
                        break;
                }
                return false;
            }
        });
    }

    private void showEmotionLayout() {
        mElEmotion.setVisibility(View.VISIBLE);
        mIvEmo.setImageResource(R.mipmap.ic_cheat_keyboard);
    }

    private void hideMoreLayout() {
        mLlMore.setVisibility(View.GONE);
    }

//    private void hideAudioButton() {
//        mBtnAudio.setVisibility(View.GONE);
//        mEtContent.setVisibility(View.VISIBLE);
//        mIvAudio.setImageResource(R.mipmap.ic_cheat_voice);
//    }

    private void showMoreLayout() {
        mLlMore.setVisibility(View.VISIBLE);
    }

    private void hideEmotionLayout() {
        mElEmotion.setVisibility(View.GONE);
        mIvEmo.setImageResource(R.mipmap.ic_cheat_emo);
    }


    private void closeBottomAndKeyboard() {
        mElEmotion.setVisibility(View.GONE);
        mLlMore.setVisibility(View.GONE);
        if (mEmotionKeyboard != null) {
            mEmotionKeyboard.interceptBackPress();
        }
    }

    /**
     * 设置switcher属性
     */
    private void setSwitcher() {
        textSwitcher.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                TextView textView = new TextView(FinalRoomActivity.this);
                textView.setTextColor(Color.parseColor("#ffffff"));
                textView.setTextSize(24);
                textView.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
                FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params.gravity = Gravity.CENTER;
                textView.setLayoutParams(params);
                return textView;
            }
        });

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    synchronized (this) {
                        handler.sendEmptyMessage(NEWS_MESSAGE_TEXT);
                        SystemClock.sleep(3000);
                    }
                }
            }
        }).start();

    }

    private void initData() {
        Intent intent = getIntent();
        roomsBean = (AllListBean.DataBean.LevelsBean.RoomsBean)
                intent.getSerializableExtra(Constant.ROOM_BEAN);
        roomNames = intent.getStringArrayExtra(Constant.ROOM_NAME);

    }

    @Override
    public boolean onEvent(IEvent iEvent) {
        return false;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:
                finish();
                break;
            case R.id.btnService:
                ToastUtils.showToast("ToDo -- service");
                break;
            case R.id.imgMenu:
                ToastUtils.showToast("ToDo --  Menu");
                break;
            case R.id.layoutRecentPeriod:

                break;
            default:
                break;
        }
    }

    @Override
    public void onEmojiSelected(String key) {
        Log.e("CSDN_LQR", "onEmojiSelected : " + key);
    }

    @Override
    public void onStickerSelected(String categoryName, String stickerName, String stickerBitmapPath) {
        Toast.makeText(getApplicationContext(), stickerBitmapPath, Toast.LENGTH_SHORT).show();
        Log.e("CSDN_LQR", "stickerBitmapPath : " + stickerBitmapPath);
    }
}
