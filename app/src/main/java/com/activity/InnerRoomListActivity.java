package com.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.MyApp;
import com.bean.AllListBean;
import com.constant.Constant;
import com.interfaces.IEvent;
import com.lottery.R;
import com.util.BitmapUtils;
import com.util.StatusBarUtil;
import com.util.ToastUtils;

import java.io.Serializable;
import java.util.List;

public class InnerRoomListActivity extends BaseActivity implements View.OnClickListener {

    private AllListBean.DataBean.LevelsBean mLevelsBean;
    private String[] roomName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inner_room_list);
        StatusBarUtil.setColor(this);
        initData();
        initUI();
    }

    private void initUI() {
        findViewById(R.id.btnBack).setOnClickListener(this);
        findViewById(R.id.btnService).setOnClickListener(this);
        TextView mTitle = findViewById(R.id.textTitle);
        mTitle.setText(roomName[1]);
        MyInnerAdapter myAdapter = new MyInnerAdapter(mLevelsBean);
        GridView mGridView = findViewById(R.id.gridView);
        mGridView.setAdapter(myAdapter);
        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(InnerRoomListActivity.this, FinalRoomActivity.class);
                AllListBean.DataBean.LevelsBean.RoomsBean roomsBean = mLevelsBean.getRooms().get(i);
                String name = roomsBean.getName();
                String[] roomNames ={roomName[0], roomName[1], name};
                intent.putExtra(Constant.ROOM_NAME, roomNames);
                intent.putExtra(Constant.ROOM_BEAN, roomsBean);
                startActivity(intent);
            }
        });
    }

    private void initData() {
        Intent intent = getIntent();
        mLevelsBean = (AllListBean.DataBean.LevelsBean)
                intent.getSerializableExtra(Constant.LEVEL_BEAN);
        roomName = intent.getStringArrayExtra(Constant.ROOM_NAME);
    }

    /**
     * 等级房间里面的vip房间
     */
    class MyInnerAdapter extends BaseAdapter {

        AllListBean.DataBean.LevelsBean roomsBean;

        public MyInnerAdapter(AllListBean.DataBean.LevelsBean roomsBean) {
            this.roomsBean = roomsBean;
        }

        @Override
        public int getCount() {
            return roomsBean.getRooms().size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View inflate = getLayoutInflater().inflate(R.layout.item_grid_vip_room, null);
            ImageView imgRoom = inflate.findViewById(R.id.imgRoom);
            BitmapUtils.getInstance().display(MyApp.getInstance(), imgRoom,
                    roomsBean.getRooms().get(i).getPicurl());
            return inflate;
        }
    }

    @Override
    public boolean onEvent(IEvent iEvent) {
        return false;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnBack:
                finish();
                break;
        }
    }
}
