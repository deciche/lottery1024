package com.activity;

import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.constant.Constant;
import com.interfaces.IEvent;
import com.lottery.R;
import com.util.CheckAvailUtil;
import com.util.DialogIos;
import com.util.SocketTask;

import java.io.OutputStream;

public class RegisterActivity extends BaseActivity implements View.OnClickListener {

    private TextView mTextGetCoder;
    private EditText mEtPhone;
    private EditText mEtCoder;
    private EditText mEtPsw;
    private EditText mEtPswAgain;
    private EditText mEtName;
    private EditText mEtRecID;
    private CheckBox mCheckAgree;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        iniUI();
    }

    private void iniUI() {
        findViewById(R.id.BtnBack).setOnClickListener(this);
        mTextGetCoder = findViewById(R.id.BtnGetCoder);
        mTextGetCoder.setOnClickListener(this);
        findViewById(R.id.TextAgreement).setOnClickListener(this);
        findViewById(R.id.BtnRegister).setOnClickListener(this);

        mEtPhone = findViewById(R.id.et_Phone);
        mEtCoder = findViewById(R.id.EditCoder);
        mEtPsw = findViewById(R.id.et_Psw);
        mEtPswAgain = findViewById(R.id.etPswAgain);
        mEtName = findViewById(R.id.EditName);
        mEtRecID = findViewById(R.id.EditRecID);

        mCheckAgree = findViewById(R.id.CheckBox);

    }

    @Override
    public boolean onEvent(IEvent iEvent) {
        return false;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.BtnBack:
                finish();
                break;
            case R.id.BtnGetCoder:
                getCoder(mTextGetCoder);
                break;
            case R.id.BtnRegister:
                doRegister();
                break;
        }
    }

    /**
     * 获取验证码
     *
     * @param mTextGetCoder
     */
    private void getCoder(final TextView mTextGetCoder) {
        if (!CheckAvailUtil.mobileNum(mEtPhone.getText().toString())) {
            DialogIos.tan(this, getResources().getString(R.string.checkPhoneNum), mEtPhone);
            return;
        }
        mTextGetCoder.setEnabled(false);
        new Handler().post(new Runnable() {
            int count = Constant.GETCODETIME;

            @Override
            public void run() {
                count--;
                if (count == 0) {
                    mTextGetCoder.setEnabled(true);
                    mTextGetCoder.setText(getResources().getString(R.string.getCoder));
                    return;
                }
                mTextGetCoder.setText(count + getResources().getString(R.string.code_reget));
                mTextGetCoder.postDelayed(this, 1000);
            }
        });
    }

    /**
     * 注册
     */
    private void doRegister() {
        String phone = mEtPhone.getText().toString();
        String coder = mEtCoder.getText().toString();
        String psw = mEtPsw.getText().toString();
        String pswAgain = mEtPswAgain.getText().toString();
        String name = mEtName.getText().toString().trim();
        String recID = mEtRecID.getText().toString().trim();

        if (!CheckAvailUtil.mobileNum(phone)) {
            DialogIos.tan(this, getResources().getString(R.string.checkPhoneNum), mEtPhone);
            return;
        }

        if (TextUtils.isEmpty(coder)) {
            DialogIos.tan(this, getResources().getString(R.string.checkCoder), mEtCoder);
            return;
        }

        if (TextUtils.isEmpty(psw)) {
            DialogIos.tan(this, getResources().getString(R.string.checkPsw), mEtPsw);
            return;
        }

        if (TextUtils.isEmpty(pswAgain)) {
            DialogIos.tan(this, getResources().getString(R.string.input_psw_again), mEtPswAgain);
            return;
        }

        if (!psw.equals(pswAgain)) {
            DialogIos.tan(this, getResources().getString(R.string.checkPswSame), mEtPsw);
            return;
        }

        if (TextUtils.isEmpty(name)){
            DialogIos.tan(this, getResources().getString(R.string.checkName), mEtName);
            return;
        }

        if (!mCheckAgree.isChecked()){
            DialogIos.tan(this, getResources().getString(R.string.checkCheckBox));
            return;
        }

        OutputStream os = SocketTask.getOS();

    }


}
