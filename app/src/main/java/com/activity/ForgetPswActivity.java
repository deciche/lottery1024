package com.activity;

import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.constant.Constant;
import com.interfaces.IEvent;
import com.lottery.R;
import com.util.CheckAvailUtil;
import com.util.DialogIos;

public class ForgetPswActivity extends BaseActivity implements View.OnClickListener {

    private TextView mTextGetCoder;
    private EditText mEtPhone;
    private EditText mEtCoder;
    private EditText mEtPsw;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_psw);
        initUI();
    }

    private void initUI() {
        findViewById(R.id.BtnBack).setOnClickListener(this);
        mTextGetCoder = findViewById(R.id.BtnGetCoder);
        mTextGetCoder.setOnClickListener(this);
        findViewById(R.id.BtnConfirm).setOnClickListener(this);

        mEtPhone = findViewById(R.id.et_Phone);
        mEtCoder = findViewById(R.id.EditCoder);
        mEtPsw = findViewById(R.id.et_Psw);
    }

    @Override
    public boolean onEvent(IEvent iEvent) {
        return false;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.BtnBack:
                finish();
                break;
            case R.id.BtnGetCoder:
                getCoder(mTextGetCoder);
                break;
            case R.id.BtnConfirm:
                confirm();
                break;
        }
    }

    private void confirm() {
        String phone = mEtPhone.getText().toString();
        String coder = mEtCoder.getText().toString();
        String psw = mEtPsw.getText().toString();

        if (!CheckAvailUtil.mobileNum(phone)) {
            DialogIos.tan(this, getResources().getString(R.string.checkPhoneNum), mEtPhone);
            return;
        }

        if (TextUtils.isEmpty(coder)) {
            DialogIos.tan(this, getResources().getString(R.string.checkCoder), mEtCoder);
            return;
        }

        if (TextUtils.isEmpty(psw)) {
            DialogIos.tan(this, getResources().getString(R.string.checkNewPsw), mEtPsw);
            return;
        }
    }


    private void getCoder(final TextView mTextGetCoder) {
        if (!CheckAvailUtil.mobileNum(mEtPhone.getText().toString())) {
            DialogIos.tan(this, getResources().getString(R.string.checkPhoneNum), mEtPhone);
            return;
        }
        mTextGetCoder.setEnabled(false);
        new Handler().post(new Runnable() {
            int count = Constant.GETCODETIME;

            @Override
            public void run() {
                count--;
                if (count == 0) {
                    mTextGetCoder.setEnabled(true);
                    mTextGetCoder.setText(getResources().getString(R.string.getCoder));
                    return;
                }
                mTextGetCoder.setText(count + getResources().getString(R.string.code_reget));
                mTextGetCoder.postDelayed(this, 1000);
            }
        });
    }
}
