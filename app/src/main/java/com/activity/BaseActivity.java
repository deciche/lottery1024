package com.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;

import com.MainPresenter;
import com.interfaces.IEventListener;
import com.lottery.R;
import com.util.SocketTask;
import com.util.StatusBarUtil;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import de.greenrobot.event.EventBus;

/**
 * Created by Mars on 2017/9/20.
 */

public abstract class BaseActivity extends FragmentActivity implements IEventListener {

    private MainPresenter mMp;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mMp = MainPresenter.getInstance();
        mMp.registerEventListener(this, this);

    }

    @Override
    protected void onDestroy() {
        mMp.unregisterEventListener(this);
        EventBus.getDefault().unregister(this);
//        InputStream is = SocketTask.getIS();
//        OutputStream os = SocketTask.getOS();
//
//        try {
//            is.close();
//            os.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        super.onDestroy();
    }
}
