package com.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;

import com.lottery.R;

public class GuideActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guide);
        initUI();
    }

    private void initUI() {
        ViewPager viewPager = findViewById(R.id.viewPagerGuide);
        viewPager.setAdapter(new GuidePagerAdapter());
    }

    private class GuidePagerAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View inflate = getLayoutInflater().inflate(R.layout.layout_guide, null);
            ImageView iv_guide = inflate.findViewById(R.id.iv_guide);
            switch (position) {
                case 0:
                    iv_guide.setBackgroundResource(R.drawable.ic_launcher);
                    break;
                case 1:
                    iv_guide.setBackgroundColor(123);
                    iv_guide.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            startActivity(new Intent(GuideActivity.this, LoginActivity.class));
                            finish();
                        }
                    });
                    break;
            }
            container.addView(inflate, LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            return inflate;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }
    }
}
