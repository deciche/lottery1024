package com.message;

/**
 * Created by Mars on 2017/9/21.
 */

public class MsgId {


    public static final short MSG_SERVER_BASE = 0;

    public static short MSG_KEEP_LIVE = MSG_SERVER_BASE + 0x14;

    public static short MSG_KEEP_LIVE_TIMEOUT = 0x15;

    //客户端消息
    public static final short MSG_CLIENT_BASE = 20000;

    //客户端告知服务端回到handler
    public static final short MSG_CLIENT_CALLBACK_HANDLER = MSG_CLIENT_BASE + 1;

    public static final short MSG_CLIENT_REQUEST = MSG_CLIENT_BASE + 2;

    public static final short MSG_CLIENT_DISCONNECT = MSG_CLIENT_BASE + 3;

    public static final short MSG_CLIENT_RECONNECT = MSG_CLIENT_BASE + 4;

    public static final short MSG_LOGIN = 1;

    public static final short MSG_GET_ALL_LIST = 8;
}
