package com.message;

import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFactory;
import org.apache.mina.filter.codec.ProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolEncoder;

import java.nio.charset.Charset;

/**
 * Created by Mars on 2017/9/21.
 */

public class UserMessageProtocolCodecFactory implements ProtocolCodecFactory {

    private final UserMessageEncoder mEncoder;
    private final UserMessageDecoder mDecoder;

    public UserMessageProtocolCodecFactory() {
        mEncoder = new UserMessageEncoder();
        mDecoder = new UserMessageDecoder();
    }

    @Override
    public ProtocolEncoder getEncoder(IoSession ioSession) throws Exception {
        return mEncoder;
    }

    @Override
    public ProtocolDecoder getDecoder(IoSession ioSession) throws Exception {
        return mDecoder;
    }
}
