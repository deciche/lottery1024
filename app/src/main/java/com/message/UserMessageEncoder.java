package com.message;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolEncoderAdapter;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;

/**
 * Created by Mars on 2017/9/21.
 */

public class UserMessageEncoder extends ProtocolEncoderAdapter {

    public UserMessageEncoder() {
    }

    @Override
    public void encode(IoSession session, Object object, ProtocolEncoderOutput output)
            throws Exception {
        if (object == null || !(object instanceof MsgPacket)) {
            return;
        }

        MsgPacket packet = (MsgPacket) object;
//        if (packet.mPackageSize != MsgHead.HEAD_SIZE)
//            return;

        IoBuffer buffer = IoBuffer.allocate(packet.mPackageSize).setAutoExpand(true);
        buffer.put(packet.array());
        buffer.flip();
        output.write(buffer);
    }
}
