package com.message;

import android.text.TextUtils;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Created by Mars on 2017/9/21.
 */

public class MsgPacket extends MsgHead {


    private String mBody = "";

    public MsgPacket(short usMsgType) {
        super(usMsgType);
    }

    public void setBody(String body) {
        this.mBody = body;
    }

    @Override
    public String toString() {
        String str = new String(array());
        return str;
    }

    public byte[] array() {
        short packageSize = (short) (MsgHead.HEAD_SIZE + mBody.getBytes().length);
        ByteBuffer bb = ByteBuffer.allocate(packageSize);
        bb.order(ByteOrder.nativeOrder());

        bb.putShort(packageSize);
        bb.putShort(mMsgType);
        bb.putShort(mPackageInstruction);
        bb.putShort(mPackageNum);

        if (!TextUtils.isEmpty(mBody))
            bb.put(mBody.getBytes());

        return bb.array();
    }
}
