package com.message;

import com.util.LogUtil;

import java.util.ArrayList;

/**
 * Created by Mars on 2017/9/26.
 */

public class SendBufferQueue<MsgPack> {
    public ArrayList<MsgPack> mArrayList = null;

    public SendBufferQueue() {
        mArrayList = new ArrayList<>();
    }

    public synchronized void push(MsgPack mp) {
        mArrayList.add(mp);
    }

    public synchronized void pushToTop(MsgPack mp) {
        mArrayList.add(0, mp);
    }

    public synchronized MsgPack pop() {
        if (mArrayList.size() == 0) {
            return null;
        }

        MsgPack mp = mArrayList.get(0);
        mArrayList.remove(mp);

        return mp;
    }

    public synchronized void clear() {
        mArrayList.clear();
    }

    public synchronized int size() {
        return mArrayList.size();
    }

    public synchronized MsgPack get(int index) {
        if (mArrayList == null || mArrayList.size() == 0) {
            return null;
        }

        return mArrayList.get(index);
    }
}
