package com.message;

import org.apache.mina.core.buffer.IoBuffer;

public interface IMsg {
    String TAG = "EVENT";

    void setSize(short usSize);

    short getSize();

    void setType(short usType);

    short getType();

    boolean parseBody(IoBuffer in);

    byte[] array();
}

