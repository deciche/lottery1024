package com.message;

/**
 * Created by Mars on 2017/9/21.
 */

public class MsgHead {

    public static final int HEAD_SIZE = 8;

    public short mPackageSize; //包长度
    public short mMsgType; //包类型
    protected short mPackageInstruction = 0;
    protected short mPackageNum; //包序号

    public MsgHead(short usMsgType) {
        init(usMsgType);
    }

    public MsgHead(short usMsgType, short packageNum) {
        init(usMsgType, packageNum);
    }

    private void init(short usMsgType) {
        this.mMsgType = usMsgType;
        this.mPackageSize = HEAD_SIZE;
    }

    private void init(short usMsgType, short packageNum) {
        this.mMsgType = usMsgType;
        this.mPackageSize = HEAD_SIZE;
        this.mPackageNum = packageNum;
    }


    public void setUcHeadSize(byte ucHeadSize) {
        this.mPackageSize = ucHeadSize;
    }

    public short getUcHeadSize() {
        return mPackageSize;
    }

    public void setType(short usMsgType) {
        this.mMsgType = usMsgType;
    }

    public short getMsgType() {
        return mMsgType;
    }

}
