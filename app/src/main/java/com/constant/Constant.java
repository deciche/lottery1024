package com.constant;

/**
 * Created by Mars on 2017/9/22.
 */

public class Constant {

    public static final long ENTER_TIME_DELAY = 1000;

    public static int GETCODETIME = 60;

    //    public static String IP = "112.126.68.31";
    //测试环境
    public static String IP = "123.57.53.75";
    public static int PORT = 8001;

    // 包的指令类型，默认为100
    public static short TYPE_INSTRUCTION = 100;


    public static String LEVEL_LIST = "levelList";
    public static String LEVEL_BEAN = "levelBean";
    public static String ROOM_NAME = "roomsName";
    public static String ROOM_BEAN = "roomsBean";
}
