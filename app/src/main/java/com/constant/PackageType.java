package com.constant;

/**
 * Created by Mars on 2017/9/27.
 */

public class PackageType {
    // 1、获取短信验证码
    public static final short GET_MSG_CODE = 1;
    // 2、用户注册
    public static final short USER_REGISTER = 2;
    // 3、用户登录
    public static final short USER_LOGIN = 3;
    // 4、请求个人资料
    public static final short GET_PERSON_DATA = 4;
    // 5、修改个人昵称
    public static final short MODIFY_PERSON_NICKNAME = 5;
    // 6、修改个人密码
    public static final short MODIFY_PERSON_PSW = 6;
    // 7、忘记密码
    public static final short FORGET_PSW = 7;
    // 8、获取所有列表（Home）
    public static final short GET_ALL_LIST = 8;
    // 9、请求彩种列表编号
    public static final short GET_LOTTERY_LIST_NUMBERING = 9;
    // 10、请求彩种资料
    public static final short GET_LOTTERY_DATA = 10;
    // 11、请求等级列表编号
    public static final short GET_LEVEL_LIST_NUMBERING = 11;
    // 12、请求等级资料
    public static final short GET_LEVEL_DATA = 12;
    // 13、请求房间列表编号
    public static final short GET_ROOM_LIST_NUMBERING = 13;
    // 14、请求房间资料
    public static final short GET_ROOM_DATA = 14;
    // 15、请求房间规则
    public static final short GET_RULES_ROOM = 15;
    // 16、请求等级人数
    public static final short GET_LEVEL_PEOPLE_NUMBER = 16;
    // 17、请求房间人数
    public static final short GET_ROOM_PEOPLE_NUMBER = 17;
    // 18、进房间
    public static final short TAG_ENTER_ROOM = 18;
    // 19、出房间
    public static final short TAG_EXIT_ROOM = 19;
    // 20、获取下一期
    public static final short GET_NEXT_ISSUE = 20;
    // 21、房间聊天
    public static final short CHAT_ROOM = 21;
    // 22、投注
    public static final short BETTING = 22;
    // 23、请求flash列表编号 好像是banner?
    public static final short GET_BANNER_LIST = 23;
    // 24、请求flash资料
    public static final short GET_BANNER_DATA = 24;
    // 25、请求注册人数
    public static final short GET_REGISTERED_NUMBER = 25;
    // 26、请求用户盈亏
    public static final short GET_PROFIT_LOSS = 26;
    // 27、请求最新期开奖号码
    public static final short GET_LATEST_AWARD_NUMBER = 27;
    // 28、请求近10期开奖号码
    public static final short GET_LATEST_10_AWARD_NUMBER = 28;
}
