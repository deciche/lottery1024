package com.bean;

/**
 * Created by Mars on 2017/10/10.
 */

public class HeadBean {
    short length;          // 包长
    short typeInstruction; // 包指令
    short msgType;         // 包类型 是否多包（0没有多包）
    short packageOrderNum; // 包序号
    private int numCurrent;
    private int numAll;

    public short getLength() {
        return length;
    }

    public void setLength(short length) {
        this.length = length;
    }

    public short getTypeInstruction() {
        return typeInstruction;
    }

    public void setTypeInstruction(short typeInstruction) {
        this.typeInstruction = typeInstruction;
    }

    public short getMsgType() {
        return msgType;
    }

    public void setMsgType(short msgType) {
        this.msgType = msgType;
    }

    public short getPackageOrderNum() {
        return packageOrderNum;
    }

    public void setPackageOrderNum(short packageOrderNum) {
        this.packageOrderNum = packageOrderNum;
    }

    public int getNumCurrent() {
        return numCurrent;
    }

    public void setCurrentNum(int numCurrent) {
        this.numCurrent = numCurrent;
    }

    public int getNumAll() {
        return numAll;
    }

    public void setAllNum(int numAll) {
        this.numAll = numAll;
    }
}
