package com.bean;

/**
 * Created by Mars on 2017/9/28.
 */

public class LoginSuccessBean {

    /**
     * id : 3
     * name : 15805942564
     * nickname : 雨中漫步
     * goldcoins : 12425.10
     * head : http://120.27.197.252:8081/Public/Uploads/User/2017-06-28/59537b817d296.png
     * pid : 0
     * code : 200
     * msg : 成功
     */

    private String id;
    private String name;
    private String nickname;
    private String goldcoins;
    private String head;
    private String pid;
    private String code;
    private String msg;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getGoldcoins() {
        return goldcoins;
    }

    public void setGoldcoins(String goldcoins) {
        this.goldcoins = goldcoins;
    }

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
