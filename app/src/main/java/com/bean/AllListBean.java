package com.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Mars on 2017/10/17.
 */


public class AllListBean {

    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable {
        /**
         * tid : 1
         * name : 北京28
         * picurl : http://120.27.197.252:8081/Public/attached/201708/1504082144.png
         * link : http://120.27.197.252:8088/index.php/home/index/instructions?id=1
         * xml : http://120.27.197.252:8081/Public/xml/pc_odds.xml
         * levels : [{"lid":"1","levelname":"初级房","picurl":"http://120.27.197.252:8081/Public/attached/201708/1504080680.png","link":"http://120.27.197.252:8088/index.php/home/index/odds?id=1","limit":"0","rooms":[{"rid":"1","name":"vip1","picurl":"http://120.27.197.252:8081/Public/attached/201708/1504080757.png","createtime":"2017-05-10 16:27:05","minbet":"10","maxbet":"5000","totalbet":"80000","ismessage":"1"},{"rid":"2","name":"vip2","picurl":"http://120.27.197.252:8081/Public/attached/201708/1504080770.png","createtime":"2017-05-10 16:27:05","minbet":"10","maxbet":"5000","totalbet":"80000","ismessage":"1"},{"rid":"3","name":"vip3","picurl":"http://120.27.197.252:8081/Public/attached/201708/1504080780.png","createtime":"2017-05-10 16:27:05","minbet":"10","maxbet":"5000","totalbet":"80000","ismessage":"0"},{"rid":"4","name":"vip4","picurl":"http://120.27.197.252:8081/Public/attached/201708/1504080792.png","createtime":"2017-05-10 16:27:05","minbet":"10","maxbet":"5000","totalbet":"80000","ismessage":"0"}]},{"lid":"2","levelname":"中级房","picurl":"http://120.27.197.252:8081/Public/attached/201708/1504080692.png","link":"http://120.27.197.252:8088/index.php/home/index/odds?id=2","limit":"0","rooms":[{"rid":"5","name":"vip1","picurl":"http://120.27.197.252:8081/Public/attached/201708/1504080837.png","createtime":"2017-05-10 16:27:05","minbet":"10","maxbet":"5000","totalbet":"80000","ismessage":"1"},{"rid":"6","name":"vip2","picurl":"http://120.27.197.252:8081/Public/attached/201708/1504080848.png","createtime":"2017-05-10 16:27:05","minbet":"10","maxbet":"5000","totalbet":"80000","ismessage":"1"},{"rid":"7","name":"vip3","picurl":"http://120.27.197.252:8081/Public/attached/201708/1504080863.png","createtime":"2017-05-10 16:27:05","minbet":"10","maxbet":"5000","totalbet":"80000","ismessage":"1"},{"rid":"8","name":"vip4","picurl":"http://120.27.197.252:8081/Public/attached/201708/1504080875.png","createtime":"2017-05-10 16:27:05","minbet":"10","maxbet":"5000","totalbet":"80000","ismessage":"1"}]},{"lid":"3","levelname":"高级房","picurl":"http://120.27.197.252:8081/Public/attached/201708/1504080706.png","link":"http://120.27.197.252:8088/index.php/home/index/odds?id=3","limit":"100000","rooms":[{"rid":"9","name":"vip1","picurl":"http://120.27.197.252:8081/Public/attached/201708/1504080886.png","createtime":"2017-05-10 16:27:05","minbet":"10","maxbet":"5000","totalbet":"80000","ismessage":"1"},{"rid":"10","name":"vip2","picurl":"http://120.27.197.252:8081/Public/attached/201708/1504080904.png","createtime":"2017-05-10 16:27:05","minbet":"10","maxbet":"5000","totalbet":"80000","ismessage":"1"},{"rid":"11","name":"vip3","picurl":"http://120.27.197.252:8081/Public/attached/201708/1504080914.png","createtime":"2017-05-10 16:27:05","minbet":"10","maxbet":"5000","totalbet":"80000","ismessage":"1"},{"rid":"12","name":"vip4","picurl":"http://120.27.197.252:8081/Public/attached/201708/1504080927.png","createtime":"2017-05-10 16:27:05","minbet":"10","maxbet":"5000","totalbet":"80000","ismessage":"1"}]}]
         */

        private String tid;
        private String name;
        private String picurl;
        private String link;
        private String xml;
        private List<LevelsBean> levels;

        public String getTid() {
            return tid;
        }

        public void setTid(String tid) {
            this.tid = tid;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPicurl() {
            return picurl;
        }

        public void setPicurl(String picurl) {
            this.picurl = picurl;
        }

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }

        public String getXml() {
            return xml;
        }

        public void setXml(String xml) {
            this.xml = xml;
        }

        public List<LevelsBean> getLevels() {
            return levels;
        }

        public void setLevels(List<LevelsBean> levels) {
            this.levels = levels;
        }

        public static class LevelsBean implements Serializable{
            /**
             * lid : 1
             * levelname : 初级房
             * picurl : http://120.27.197.252:8081/Public/attached/201708/1504080680.png
             * link : http://120.27.197.252:8088/index.php/home/index/odds?id=1
             * limit : 0
             * rooms : [{"rid":"1","name":"vip1","picurl":"http://120.27.197.252:8081/Public/attached/201708/1504080757.png","createtime":"2017-05-10 16:27:05","minbet":"10","maxbet":"5000","totalbet":"80000","ismessage":"1"},{"rid":"2","name":"vip2","picurl":"http://120.27.197.252:8081/Public/attached/201708/1504080770.png","createtime":"2017-05-10 16:27:05","minbet":"10","maxbet":"5000","totalbet":"80000","ismessage":"1"},{"rid":"3","name":"vip3","picurl":"http://120.27.197.252:8081/Public/attached/201708/1504080780.png","createtime":"2017-05-10 16:27:05","minbet":"10","maxbet":"5000","totalbet":"80000","ismessage":"0"},{"rid":"4","name":"vip4","picurl":"http://120.27.197.252:8081/Public/attached/201708/1504080792.png","createtime":"2017-05-10 16:27:05","minbet":"10","maxbet":"5000","totalbet":"80000","ismessage":"0"}]
             */

            private String lid;
            private String levelname;
            private String picurl;
            private String link;
            private String limit;
            private List<RoomsBean> rooms;

            public String getLid() {
                return lid;
            }

            public void setLid(String lid) {
                this.lid = lid;
            }

            public String getLevelname() {
                return levelname;
            }

            public void setLevelname(String levelname) {
                this.levelname = levelname;
            }

            public String getPicurl() {
                return picurl;
            }

            public void setPicurl(String picurl) {
                this.picurl = picurl;
            }

            public String getLink() {
                return link;
            }

            public void setLink(String link) {
                this.link = link;
            }

            public String getLimit() {
                return limit;
            }

            public void setLimit(String limit) {
                this.limit = limit;
            }

            public List<RoomsBean> getRooms() {
                return rooms;
            }

            public void setRooms(List<RoomsBean> rooms) {
                this.rooms = rooms;
            }

            public static class RoomsBean implements Serializable{
                /**
                 * rid : 1
                 * name : vip1
                 * picurl : http://120.27.197.252:8081/Public/attached/201708/1504080757.png
                 * createtime : 2017-05-10 16:27:05
                 * minbet : 10
                 * maxbet : 5000
                 * totalbet : 80000
                 * ismessage : 1
                 */

                private String rid;
                private String name;
                private String picurl;
                private String createtime;
                private String minbet;
                private String maxbet;
                private String totalbet;
                private String ismessage;

                public String getRid() {
                    return rid;
                }

                public void setRid(String rid) {
                    this.rid = rid;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public String getPicurl() {
                    return picurl;
                }

                public void setPicurl(String picurl) {
                    this.picurl = picurl;
                }

                public String getCreatetime() {
                    return createtime;
                }

                public void setCreatetime(String createtime) {
                    this.createtime = createtime;
                }

                public String getMinbet() {
                    return minbet;
                }

                public void setMinbet(String minbet) {
                    this.minbet = minbet;
                }

                public String getMaxbet() {
                    return maxbet;
                }

                public void setMaxbet(String maxbet) {
                    this.maxbet = maxbet;
                }

                public String getTotalbet() {
                    return totalbet;
                }

                public void setTotalbet(String totalbet) {
                    this.totalbet = totalbet;
                }

                public String getIsmessage() {
                    return ismessage;
                }

                public void setIsmessage(String ismessage) {
                    this.ismessage = ismessage;
                }
            }
        }
    }
}