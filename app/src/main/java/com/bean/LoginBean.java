package com.bean;

import com.interfaces.IEvent;

/**
 * Created by Mars on 2017/9/27.
 */

public class LoginBean implements IEvent {

    /**
     * user : 15805942564
     * password : 123456
     */

    private String user;
    private String password;

    public LoginBean(String user, String password) {
        this.user = user;
        this.password = password;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
