package com.bean;

/**
 * Created by Mars on 2017/9/28.
 */

public class PackageBean {

    HeadBean headBean;
    byte[] body;
    private String json;

    public HeadBean getHeadBean() {
        return headBean;
    }

    public void setHeadBean(HeadBean headBean) {
        this.headBean = headBean;
    }

    public byte[] getBody() {
        return body;
    }

    public void setBody(byte[] body) {
        this.body = body;
    }


    public void setBodyJson(String json) {
        this.json = json;
    }

    public String getBodyJson() {
        return json;
    }
}
