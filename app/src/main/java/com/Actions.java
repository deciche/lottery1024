package com;

import com.message.MsgId;

/**
 * Created by Mars on 2017/9/25.
 */

public class Actions {

    private MainPresenter mMP = MainPresenter.getInstance();
    private static Actions mActions = new Actions();

    public static Actions getInstance() {
        return mActions;
    }

    // 登录
    public void doLogin(String body) {
        short usMsgType = MsgId.MSG_LOGIN;
        mMP.sendRequestMessage2Service(usMsgType, body);
    }

    // 获取所有信息
    public void getAllList() {
        short packageType = MsgId.MSG_GET_ALL_LIST;
//        mMP.sendRequestMessage2Service();
    }
}
