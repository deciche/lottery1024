package com.interfaces;

/**
 * Created by Mars on 2017/9/20.
 */

public abstract interface IEventListener {

    abstract boolean onEvent(IEvent iEvent);

}
