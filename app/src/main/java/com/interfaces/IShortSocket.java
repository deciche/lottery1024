package com.interfaces;

/**
 * Created by Mars on 2017/9/25.
 */

public interface IShortSocket {
    void getData(String json);

    void getDataError();

    void connectError();
}
