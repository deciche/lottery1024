package com.mina;

import com.util.LogUtil;

import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;

/**
 * Created by Mars on 2017/10/10.
 */

public class MyServerHandler extends IoHandlerAdapter {

    public MyServerHandler() {
    }

    @Override
    public void sessionCreated(IoSession session) throws Exception {
        LogUtil.e("-- sessionCreated");
    }

    @Override
    public void sessionOpened(IoSession session) throws Exception {
        LogUtil.e("-- sessionOpened");
    }

    @Override
    public void sessionClosed(IoSession session) throws Exception {
        LogUtil.e("-- sessionClosed");
    }

    @Override
    public void sessionIdle(IoSession session, IdleStatus status) throws Exception {
        LogUtil.e("-- sessionIdle");
    }

    @Override
    public void exceptionCaught(IoSession session, Throwable cause) throws Exception {
        LogUtil.e("-- exceptionCaught");
    }

    @Override
    public void messageReceived(IoSession session, Object message) throws Exception {
        LogUtil.e("-- messageReceived");
    }

    @Override
    public void messageSent(IoSession session, Object message) throws Exception {
        LogUtil.e("-- messageSent");
    }
}
